FROM python:3.10.2

RUN pip install poetry

CMD ["poetry", "--version"]
